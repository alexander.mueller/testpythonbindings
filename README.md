<!--
SPDX-FileCopyrightInfo: Copyright © DUNE Project contributors, see file LICENSE.md in module root
SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception
-->

# Tests

Test python bindings and dune-py locally
```shell
docker container run -it -u root --entrypoint /bin/bash ikarusproject/ikarus-dev:latest
cd tmp 
git clone https://gitlab.dune-project.org/alexander.mueller/testpythonbindings.git 
cd testpythonbindings 
mkdir build-cmake 
cd build-cmake
cmake ..
cmake --build . --parallel 2 --target _foo 
ctest --verbose
```

Upload package to testpypi
```shell
/dune/dune-common/build-cmake/run-in-dune-env pip install twine scikit-build
git config --global --add safe.directory /tmp/testpythonbindings

cat << EOF > ~/.pypirc
[distutils]
index-servers = testpypi
[testpypi]
username = __token__
password = pypi-AgENdGVzdC5weXBpLm9yZwIkNWIyZmI3MTktNGZkMy00MDZlLWFiOTYtNjVjMzllMGRhODYzAAIQWzEsWyJweWZvb2ZvbyJdXQACLFsyLFsiMTBjOWYwNGQtYmM4Yi00YWU1LWE5YWUtNWMwOWU5YjQ3ZTdmIl1dAAAGINhZzJCeIFTyVaMMRWFwTDpwQeojWLQfV1jGp7ARaGTR
EOF

# increase version in setup.py pyfoofooVersion
/dune/dune-common/build-cmake/run-in-dune-env python setup.py sdist
/dune/dune-common/build-cmake/run-in-dune-env python -m twine upload  --repository testpypi dist/* --verbose
```

Test downloaded package from testpypi

```shell
docker container run -it -u root --entrypoint /bin/bash ikarusproject/ikarus-dev:latest
/dune/dune-common/build-cmake/run-in-dune-env pip install twine scikit-build
/dune/dune-common/build-cmake/run-in-dune-env python -m pip install --extra-index-url https://test.pypi.org/simple/ pyfoofoo --no-build-isolation --verbose

cd tmp
cat << EOF > test.py
import os
import logging
os.environ['DUNE_LOG_LEVEL'] = 'debug'
os.environ['DUNE_SAVE_BUILD'] = 'console'
logger = logging.getLogger(__name__)
logger.setLevel(10)
import dune.geometry
dune.geometry.referenceElement(dune.geometry.triangle)
from dune.common import *
import foo
assert foo.add(3,4)==7

v = FieldVector([0, 1, 2])

bard =foo.bardouble()
print(bard.func1(7.3))

bari =foo.barint()
print(bari.func1(3))
EOF

/dune/dune-common/build-cmake/run-in-dune-env python test.py
```
