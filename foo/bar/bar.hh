// SPDX-FileCopyrightText: Copyright © DUNE Project contributors, see file COPYING in module root
// SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception

#pragma once

#include <dune/common/classname.hh>
#include <string>



namespace Foo {
template <class T>
struct Bar {
  using Type=T;
  std::string func1(Type t) {

    return Dune::className<Bar>() +" " std::to_string(t);
  }

};

}
