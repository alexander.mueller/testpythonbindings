// SPDX-FileCopyrightText: Copyright © DUNE Project contributors, see file COPYING in module root
// SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception

#include <foo/foo.hh>
namespace Foo
{

  int foo(int v){ return v*v;};
// clang-format on
}
