// SPDX-FileCopyrightText: 2023  mueller@ibb.uni-stuttgart.de
// SPDX-License-Identifier: LGPL-3.0-or-later
#pragma once

namespace Foo
{

  int foo(int v);
}
