// SPDX-FileCopyrightText: Copyright © DUNE Project contributors, see file COPYING in module root
// SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception

#pragma once

#include <dune/python/common/typeregistry.hh>
#include <dune/python/pybind11/functional.h>
#include <dune/python/pybind11/pybind11.h>
#include <dune/python/pybind11/stl.h>

#include <foo/bar/bar.hh>


namespace Foo::Python {

// Python wrapper for the FVAssembler C++ class
template <class Bar, class... options>
void registerBar(pybind11::handle scope, pybind11::class_<Bar, options...> cls) {
  using pybind11::operator""_a;

  using Type = typename Bar::Type;

  cls.def(pybind11::init([]() {
    return new Bar();
  }));

  cls.def(
      "func1",
      [](Bar& self, Type t) {
        return self.func1(t);
      });

}

}  // namespace Ikarus::Python
