# SPDX-FileCopyrightText: Copyright © DUNE Project contributors, see file LICENSE.md in module root
# SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception



import numpy
from dune.common import *
import os
import pwd
import sys
import setpath


setpath.set_path()


import foo




if __name__ == "__main__":

    assert foo.add(3,4)==7

    v = FieldVector([0, 1, 2])

    bard =foo.bardouble()
    print(bard.func1(7.3))


    bari =foo.barint()
    print(bari.func1(3))
