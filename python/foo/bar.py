# SPDX-FileCopyrightText: Copyright © DUNE Project contributors, see file COPYING in module root
# SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception

from dune.generator.generator import SimpleGenerator
from dune.common.hashit import hashIt
import dune



def bardouble() :
    generator = SimpleGenerator("Bar", "Foo::Python")
    element_type = f"Foo::Bar<double>"

    includes = []
    includes += ["foo/python/bar/bar.hh"]
    moduleName = "Bar_" + hashIt(element_type)
    module = generator.load(
        includes=includes,
        typeName=element_type,
        moduleName=moduleName
    )
    return module.Bar()


def barint() :
    generator = SimpleGenerator("Bar", "Foo::Python")
    element_type = f"Foo::Bar<int>"

    includes = []
    includes += ["foo/python/bar/bar.hh"]
    moduleName = "Bar_" + hashIt(element_type)
    module = generator.load(
        includes=includes,
        typeName=element_type,
        moduleName=moduleName
    )
    return module.Bar()
