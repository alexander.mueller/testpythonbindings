# SPDX-FileCopyrightText: Copyright © DUNE Project contributors, see file COPYING in module root
# SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception
# this file is modified from dumux https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/setup.py

try:
    from dune.packagemetadata import metaData
except ImportError:
    from packagemetadata import metaData
from skbuild import setup



pyfoofooVersion = "0.3.0.dev202310000088" #+ datetime.today().time().strftime("%H%M%S")
duneVersion = "2.9.0"

metadata = metaData(duneVersion)[1]
metadata["version"] = pyfoofooVersion

print(metadata)
metadata["name"] = "pyfoofoo"

# from setuptools import setup, find_packages


setup(**metadata)
